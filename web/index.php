<?php

use Symfony\Component\Debug\Debug;

// Serve static files
$filename = __DIR__.preg_replace('#(\?.*)$#', '', $_SERVER['REQUEST_URI']);
if (php_sapi_name() === 'cli-server' && is_file($filename)) {
    return false;
}

// Treat dynamic pages
require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app = require __DIR__.'/../src/app.php';
require __DIR__.'/../config/config.php';
require __DIR__.'/../src/controllers.php';
$app->run();
?>
