![Diagonal][logo]
# DIAGONAL-PHP #

Diagonal-php is a [Silex PHP](https://silex.symfony.com) web app for diagnostics using Diagonal format.

## What is it ? ##

Diagnostic is the identification of the nature and cause of a certain phenomenon.
It is typically used to determine the causes of symptoms, mitigations, and solutions.

Diagnostic is composed of questions and answers, and is recorded in a special YAML format called Diagonal (DIAGnostic ON Application Layer).
All those documents are composing a knowledge base.
It was created mainly for computer science, but can be used in any sector.
See Wiki for [format documentation](https://bitbucket.org/crownedgrouse/diagonal-php/wiki/Diagonal%20format) and tutorial.

Diagonal-php is managing those documents without database but with YAML indexes.
It is then really easy to use, to backup, to move...

## Getting started ##

- Make sure that php-cli is installed
   `php -v`
- Clone project
   `git clone https://bitbucket.org/crownedgrouse/diagonal-php`
- Go into repository
   `cd diagonal-php`
- Install [composer](https://getcomposer.org/download/)
- Fetch dependencies
   `php composer.phar update`
- Go into config directory
   `cd config`
- Copy all sample `*.dist` files by removing `.dist` suffix.

## Configure ##
Before going further it is needed to configure `diagonal-php` .

Edit `config/config.php`.

Here are the available parameters :

   Variable                 | Type           | Comment                                  | Default
----------------------------|----------------|------------------------------------------|---------------------------------------------------
'diagonal.path'             | String         | Path to application data and database    | `__DIR__`.'/../app'
'diagonal.auth.method'      | Array          | Authentication method (file or radius)   | array('file' => array('path' => `__DIR__`.'/../config/auth.yaml'))
'diagonal.markdown.flavor'  | String         | Markdown type (traditional,extra, github)| traditional
'diagonal.namespaces'       | Array          | List of local namespaces owned           | array()

See [Wiki](https://bitbucket.org/crownedgrouse/diagonal-php/wiki/Configuration) for more details.

## Serve pages ##
Then, there is several ways to serve PHP pages.
The easiest, but probably reserved for testing and developping, is to simply run :

```
#!bash
php -S localhost:8080 -t web web/index.php

```
Web app is then available in your browser at `http://localhost:8080` .

Stop with `Ctrl+C`. For launching it in background, simply add `&` to command.

For production installation, see [other ways to serve pages](https://silex.symfony.com/doc/2.0/web_servers.html).

(Note : Port number under 1024 need root privileges)

[logo]: https://bytebucket.org/crownedgrouse/diagonal-php/raw/215139d979d3a629bdf043b03c747f6b0d042f62/web/img/diagonal.png "Diagonal"




