<?php

use Symfony\Component\HttpFoundation\Request;

// Upload diag file
$app->match('/upload', function (Request $request) use ($app){

    $form = $app['form.factory']
        ->createBuilder('form')
        ->add('FileUpload', 'file')
        ->getForm()
    ;

    $request = $app['request'];
    $message = 'Upload a file';

    $color = "blue" ;

    if ($request->isMethod('POST')) {

        $form->bind($request);

        if ($form->isValid()) {
            $files = $request->files->get($form->getName());
            /* Make sure that Upload Directory is properly configured and writable */
            $D = new Diagonal($app['diagonal.path']);
            $path = $D->tmp_path ;
            $filename = $files['FileUpload']->getClientOriginalName();
            $tmpname = tempnam($path, "doc");
            $files['FileUpload']->move($path,$tmpname);
            $valid = true ;
            $color = "green" ;

            if($D->check_yaml($tmpname,true)===false){
               $valid = false ;
               $color = "red" ;
               $message = "Invalid YAML file : ".$D->error ;
            }elseif($D->is_valid_diagonal($tmpname)===false){
               $valid = false ;
               $color = "red" ;
               $message = "Invalid Diagonal file : ".$D->error ;
            }elseif($D->insert_doc($tmpname)===false){
               $valid = false ;
               $color = "red" ;
               $message = "Cannot insert document : ".$D->error ;
            }
            $D->data=array() ; //reset cache

            if($valid === true) $message = 'File was successfully uploaded!';
        }
    }

    $response =  $app['twig']->render(
        'upload.html.twig',
        array(
            'message' => $message,
            'color' => $color,
            'form' => $form->createView()
        )
    );

    return $response;

}, 'GET|POST');

?>
