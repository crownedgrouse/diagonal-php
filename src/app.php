<?php

use Silex\Application;
use Silex\Provider\FormServiceProvider;

$app = new Application();

$app->register(new FormServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../templates',
    'twig.options' => array('debug' => true),
));

$app->register(new Silex\Provider\TranslationServiceProvider(), array(
    'translator.messages' => array(),
));

$app->register(new Silex\Provider\SessionServiceProvider());

return $app;

?>
