<?php

use Symfony\Component\HttpFoundation\Request;

//******************************************************************************
//**** EDIT                                                                 ****
//******************************************************************************
$app->get('/edit', function () use ($app) {
    return "Edit a new document";
});

$app->get('/edit/{ns}', function ($ns) use ($app) {
    return "Enable/disable a document in namespace $ns";
});


$app->match('/edit/{ns}/{doc}', function ($ns, $doc, Request $request) use ($app) {
   $D = new Diagonal($app['diagonal.path']);
   $namespace = $D->get_namespace_from_hashns($ns);

   if ($request->isMethod('POST')) {   // A draft is submitted
         $draft = $request->request->get('draft');
         $draft_file = tempnam("/tmp","diagonal");
         file_put_contents($draft_file, $draft);

         $valid=$D->is_valid_diagonal($draft_file);
         if($valid){
            $error="";
            $highlight = $D->markdown2html("```YAML\n".$draft."```", $app['diagonal.markdown.flavor']);
            // Store draft in temporary dir, while not published
            $draft_dir = $D->tmp_path."/$ns" ;
            @mkdir($draft_dir);
            rename($draft_file, $draft_dir."/$doc");

         }else{
            $error = $D->error ;
            $highlight = "";
         }
   }else{ // Edit new empty document
      $valid = true ;
      $draft = $app['twig']->render('diagonal.twig',  array('context' => "/diag/$ns/$doc",
                                                         'namespace' => "$namespace",
                                                         'document'  => "$doc",
                                                         'title'     => "",
                                                         'lang'      => ""));
      $highlight = $D->markdown2html("```YAML\n".$draft."```", $app['diagonal.markdown.flavor']);

   }

   return $app['twig']->render('edit.html.twig',  array('context'   => "/diag/$ns/$doc",
                                                        'namespace' => "$ns",
                                                        'document'  => "$doc",
                                                        'rights'    => $rights,
                                                        'draft'     => $draft,
                                                        'highlight' => $highlight,
                                                        'valid'     => $valid,
                                                        'error'     => $error));
}, 'GET|POST');


?>
