<?php


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

require __DIR__.'/Class/Diagonal.php';
require __DIR__.'/app_controller.php';
require __DIR__.'/diag_controller.php';
require __DIR__.'/list_controller.php';
require __DIR__.'/edit_controller.php';
require __DIR__.'/api_controller.php';
require __DIR__.'/login_controller.php';
require __DIR__.'/upload_controller.php';
require __DIR__.'/error_controller.php';


?>
