<?php
//******************************************************************************
//**** LIST                                                                 ****
//******************************************************************************
$app->get('/list', function () use ($app) {
   // Update the namespace yaml index with those infos in app/data/ns/
   $D = new Diagonal($app['diagonal.path']);
   $ns_index = $D->data_path.'/ns/namespaces.yaml' ;
   $namespaces = $D->safe_read_doc($ns_index) ;
   return $app['twig']->render('ns.html.twig', array('namespaces' => $namespaces));
});

$app->get('/list/{ns}', function ($ns) use ($app) {
   $D = new Diagonal($app['diagonal.path']);
   $doc_index =  $D->data_path."/ns/$ns/documents.yaml" ;
   $docs = $D->safe_read_doc($doc_index) ;
   // Get the document index
   $tmp = $docs['bysha1'] ;
   while (list($a, $b) = each($tmp)){
      $documents[$b['ns']][] = array_merge($b, array('nskey'=>$ns)) ;
   }
   return $app['twig']->render('doc.html.twig', array('documents' => $documents));
});

$app->put('/list/{ns}/{doc}', function ($ns, $doc) use ($app) {
    return "List questions in $doc in namespace $ns";
});

$app->get('/doc', function () use ($app) {
   // Update the namespace yaml index with those infos in app/data/ns/
   $D = new Diagonal($app['diagonal.path']);
   $ns_index = $D->data_path.'/ns/namespaces.yaml' ;
   $namespaces = $D->safe_read_doc($ns_index) ;

   while (list($k, $v) = each($namespaces)){
      $doc_index =  $D->data_path."/ns/$k/documents.yaml" ;
      $docs = $D->safe_read_doc($doc_index) ;
      if(!is_array($docs) or !isset($docs['bysha1'])) continue;
      // Get the document index
      $tmp = $docs['bysha1'] ;
      while (list($a, $b) = each($tmp)){
         $documents[$b['ns']][] = array_merge($b, array('nskey'=>$k)) ;
      }
   }

   return $app['twig']->render('doc.html.twig', array('documents' => $documents));
});

?>
