<?php

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Dumper;
use Symfony\Component\Yaml\Exception\ParseException;

use RomaricDrigon\MetaYaml\MetaYaml;
use RomaricDrigon\MetaYaml\Loader\YamlLoader;

class Diagonal {

   public function __construct($path = __DIR__."/../../app")
   {
        $this->data_path = $path."/data";
        $this->schema_path = $path."/schema";
        $this->tmp_path = $this->data_path."/tmp";
        $this->cache_path = $this->data_path."/cache";
        $this->ns_path  = $this->data_path."/ns";
        $this->error = "";
        $this->data = array();
        $this->status = array();
        @mkdir($this->ns_path,true);
        @mkdir($this->tmp_path);
        @mkdir($this->cache_path);
        @mkdir($this->schema_path);
        if(!is_writable($this->tmp_path)){
            throw new Exception("Cannot write in Diagonal temporary directory : ".$this->tmp_path);
        }
        if(!is_writable($this->cache_path)){
            throw new Exception("Cannot write in Diagonal cache directory : ".$this->cache_path);
        }
        if(!is_writable($this->ns_path)){
            throw new Exception("Cannot write in Diagonal namespaces directory : ".$this->ns_path);
        }
   }

   public function authenticate($login=NULL, $password=NULL){
      if($login == NULL) return false ;
      if($password == NULL) return false ;
      if($this->auth_method == ''){
         throw new Exception("Cannot find any authentication method in config. See \$app['diagonal.auth.method'] directive documentation.");
         return false ;
      }
      // If an array bring key as method
      if(!is_array($this->auth_method)){
         throw new Exception("Invalid value for \$app['diagonal.auth.method'] directive in config. Array expected.");
         return false ;
      }
      // Add other authentication methods here if needed
      try {
         if(isset($this->auth_method['radius'])){
            return $this-> auth_radius($login, $password, $this->auth_method['radius']);
         }elseif(isset($this->auth_method['file'])){
            return $this-> auth_file($login, $password, $this->auth_method['file']);
         }else{
               throw new Exception("Invalid authentication method in config.");
         }
       } catch (Exception $e) {
            throw new Exception($e->getMessage());
      }
      return false ;
   }

   private function client_ip(){
      if(!isset($_SERVER['HTTP_CLIENT_IP']))$_SERVER['HTTP_CLIENT_IP']= $_SERVER['REMOTE_ADDR'] ;
      if(!isset($_SERVER['HTTP_X_FORWARDE‌​D_FOR'])) $_SERVER['HTTP_X_FORWARDE‌​D_FOR']= $_SERVER['REMOTE_ADDR'] ;
      return $_SERVER['HTTP_CLIENT_IP']?:($_SERVER['HTTP_X_FORWARDE‌​D_FOR']?:$_SERVER['REMOTE_ADDR']);
   }

   private function auth_file($login, $password, $params = array()){
      $ip = $this->client_ip();
      $credfile = $params['path'];// Test if a valid Yaml file
      try {
            $yaml = Yaml::parse(file_get_contents($credfile));
            if(isset($yaml[$login])){
               $pw = explode(':', $yaml[$login]);
               $log = "(i) AUTH - $ip - Access allowed for user : $login" ;
               $ok=false;
               if($pw[0]=="sha1" and sha1("$password") == $pw[1]){
                  $ok=true;
               }elseif($pw[0]=="md5" and md5("$password") == $pw[1]){
                  $ok=true;
               }elseif($yaml[$login] == "$password"){
                  $ok=true;
               }
               if($ok){
                  error_log($log);
                  return true;
               }
            }
      } catch (ParseException $e) {
            throw new Exception("Parse error in $credfile : ".$e->getMessage());
      }
      error_log("[!] AUTH - $ip - Access denied for user : $login");
      return false ;
   }

   private function auth_radius($login, $password, $params = array()){
      if(!function_exists('radius_auth_open')){
         throw new Exception("Cannot use RADIUS method. Need 'php-radius' (PECL) to be installed.") ;
         return false ;
      }
      $ip = $this->client_ip();

      $ip_address    = $params['hostname'] ;
      $port          = (int) $params['port'] ;
      $shared_secret = $params['secret'] ;
      $timeout       = (int) $params['timeout'] ;
      $max_tries     = (int) $params['max_tries'] ;

      $radius = radius_auth_open();
      radius_add_server($radius, $ip_address, $port, $shared_secret, $timeout, $max_tries);
      radius_create_request($radius, RADIUS_ACCESS_REQUEST);
      radius_put_attr($radius, RADIUS_USER_NAME, $login);
      radius_put_attr($radius, RADIUS_USER_PASSWORD, $password);

      $result = radius_send_request($radius);

      switch ($result) {
         case RADIUS_ACCESS_ACCEPT:
            // An Access-Accept response to an Access-Request indicating that the RADIUS server authenticated the user successfully.
            error_log("(i) RADIUS - $ip - Access allowed for user : $login");
            return true ;
            break;
         case RADIUS_ACCESS_REJECT:
            // An Access-Reject response to an Access-Request indicating that the RADIUS server could not authenticate the user.
            error_log("[!] RADIUS - $ip - Access denied for user : $login");
            return false ;
            break;
         case RADIUS_ACCESS_CHALLENGE:
            // An Access-Challenge response to an Access-Request indicating that the RADIUS server requires further information in another Access-Request before authenticating the user.
            error_log("[!] RADIUS - $ip - Server $ip_address requires a 'RADIUS_ACCESS_CHALLENGE' for user : $login");
            return false ;
            break;
         default:
            throw new Exception('[!] RADIUS - Error has occurred: ' . radius_strerror($radius));
      }
      return false ;
   }

   public function get_rights($user){
      $rfile = __DIR__.'../../config/rights.yaml' ;
      $rights = $this->safe_read_doc($rfile);
      return  $rights[$user] ;
   }

   public function safe_read_doc($path){
      $val = array();
      $fp = @fopen("$path", "r+");
      if (is_resource($fp) and flock($fp, LOCK_EX)) {
         // Read content
         // Parse content
         $val = Yaml::parse(file_get_contents($path));
         flock($fp, LOCK_UN);    // release the lock
      }
      return $val ;
   }

   public static function list_texts($val){
      if(isset($val['text'])){
         return true ;
      }
      return false ;
   }

   public function replace_non_strings($v){
      if(is_array($v)){
         return implode(", ",$v) ;
      }
      return $v ;
   }

   public function get_doc_sha1($ns='', $doc='0'){
      $doc_index = $this->data_path."/ns/$ns/documents.yaml" ;
      $docs = $this->safe_read_doc($doc_index) ;
      // Get the document index
      $tmp = $docs['byid'] ;
      $docsha1 = $tmp[$doc] ;
      return $docsha1;
   }

   public function get_doc_title($ns='', $doc='0'){
      $doc_index = $this->data_path."/ns/$ns/documents.yaml" ;
      $docs = $this->safe_read_doc($doc_index) ;
      // Get the document index
      $tmp = $docs['byid'] ;
      $docsha1 = $tmp[$doc] ;
      return $docs['bysha1'][$docsha1]['title'];
   }


   public function get_nshash_from_prefix($ns, $sha1, $nspref){
      $docpath = $this->data_path."/ns/$ns/$docsha1" ;
      $docarray = $this->safe_read_doc($docpath) ;
      $namespaces = $docarray['namespaces'] ;
      $ns = $namespaces[$nspref] ;
      return $this->get_hash_ns($ns) ;
   }

   public function check_yaml($path = NULL, $cache = false){
      if($path===NULL){
         $this->error = "File not found" ;
         return false ;
      }
      // Test if a valid Yaml file
      try {
            $yaml = Yaml::parse(file_get_contents($path));
      } catch (ParseException $e) {
            $message = sprintf("Parse error: %s", $e->getMessage());
            $this->error= $message ;
            return false ;
      }
      if($yaml === NULL){
         $this->error = "Parse error: ".$e->getMessage();
         return false ;
      }
      if($cache){
         $this->data["$path"] = $yaml ;
         return true ;
      }
      return $yaml;
   }

   public function is_valid_diagonal($path=NULL, $cache = false){
      if($path===NULL){
         $this->error = "File not found" ;
         return false ;
      }
      /* Test if a valid Diagonal file */
      if($cache) $this->status["$path"]=false; // mark not ok before tests if cache required

      // First check if header is ok
      if($this->diag_check_header($path)===false){
         return false ;
      // Check if questions and answers are ok
      }
      elseif($this->diag_check_body($path)===false){
         return false ;
      }
      // Once arrived there, mark this file ok if cache required
      if($cache) $this->status["$path"]=true;
      return true ;
   }

   public function insert_doc($path = NULL, $yaml = NULL){
      if($path===NULL){
         $this->error = "File not found" ;
         return false ;
      }
      if($yaml ===NULL and !isset($this->data["$path"])){
         $yaml = $this->check_yaml($path);
      }else{
         $yaml = $this->data["$path"] ; // use cache
      }
      if(!is_array($yaml)){
         $this->error = "Invalid YAML content" ;
         return false ;
      }
      if((!isset($this->status["$path"]) and $this->is_valid_diagonal($path)=== false)
          or $this->status["$path"]===false){
         $this->error = "Invalid Diagonal content" ;
         return false ;

      }
      // Extract 'doc' entry
      $doc = $yaml['doc'] ;

      // Compute sha1sum for file and short checksum for namespace
      $short_ns = $this->get_hash_ns($doc['ns']) ;
      $sha1sum  = sha1_file($path) ;

      // Move tmpfile to app/data/ns/{short_ns}/{sha1sum}
      $doc_dir = $this->data_path.'/ns/'.$short_ns  ;
      mkdir($doc_dir, 0770, true);

      rename($path, $doc_dir.'/'.$sha1sum);

      // Update the namespace yaml index with those infos in app/data/ns/
      $ns_index = $this->data_path.'/ns/namespaces.yaml' ;
      touch($ns_index);

      // Open lock exclusive namespace index
      $fp = fopen("$ns_index", "r+");
      if (flock($fp, LOCK_EX)) {

            // Read content
            // Parse content
            $ns = Yaml::parse(file_get_contents($ns_index));

            // Add new entry
            $ns["$short_ns"] = trim($doc['ns']) ;

            // Sort keys
            ksort($ns);

            // Dump data
            $dumper = new Dumper();
            $ns_yaml = $dumper->dump($ns, 2);

            file_put_contents("$ns_index", $ns_yaml);

            // Update the documents yaml index with those infos in app/data/ns/{short_ns}/
            $doc_index = $this->data_path.'/ns/'.$short_ns.'/documents.yaml' ;
            touch($doc_index);

            // Open lock exclusive namespace index
            $fd = fopen("$doc_index", "r+");
            if (flock($fd, LOCK_EX)) {

               // Read content
               // Parse content
               $docs = Yaml::parse(file_get_contents($doc_index));

               // Add new entry
               $docs['bysha1']["$sha1sum"] = $doc ;
               $docs['byid'][$doc['id']] = "$sha1sum" ;

               // Sort keys
               ksort($docs);

               // Dump data
               $dumper2 = new Dumper();
               $doc_yaml = $dumper2->dump($docs, 3);

               file_put_contents("$doc_index", $doc_yaml);

               flock($fd, LOCK_UN);    // release the lock
            }
            flock($fp, LOCK_UN);    // release the lock
            return true ;
      }
      $this->error = "Unable to lock index file" ;
      return false ;
   }

   private function diag_check_header($path=NULL){
      if($path===NULL){
         return false ;
      }
      $schema = $this->schema_path."/header.yaml";
      $loader = new YamlLoader();
      try {
         $schema = new MetaYaml($loader->loadFromFile($schema));
         $schema->validate($loader->loadFromFile($path));
      }catch (Exception $e){
         $this->error= $e->getMessage() ;
         return false ;
      }
      return true;
   }

   private function diag_check_body($path=NULL){
      if($path===NULL){
         return false ;
      }
      // remove header
      $yaml = $this->check_yaml($path);
      unset($yaml['namespaces']);
      unset($yaml['doc']);
      // Dump data
      $dumper = new Dumper();
      $doc_body = $dumper->dump($yaml, 2);
      $path_body = "$path.body";
      file_put_contents("$path_body", $doc_body);

      $schema = $this->schema_path."/body.yaml";
      $loader = new YamlLoader();
      try {
         $schema = new MetaYaml($loader->loadFromFile($schema));
         $schema->validate($loader->loadFromFile($path_body));
      }catch (Exception $e){
         $this->error= $e->getMessage() ;
         return false ;
      }finally{
         @unlink($path_body);
      }
      //*** Do extra checks that cannot be done in schema validator ***
      // Check that all entries have valid names, either ? or  ^
      foreach ($yaml as $key => $value) {
         if($key[0] !== "?" and $key[0] !== "^"){
            $this->error = "Invalid question or diagnostic key : $key" ;
            return false ;
         }
      }
      // TODO extract all links to documents and check they are existing

      return true;
   }

   public function markdown2html($m, $flavor = 'traditional'){

      switch($flavor){
         case 'github':
            // Github Flavored Markdown:
            $parser = new \cebe\markdown\GithubMarkdown();
            break;
         case 'extra':
            // Markdown Extra:
            $parser = new \cebe\markdown\MarkdownExtra();
            break;
         default :
            // Traditional Markdown:
            $parser = new \cebe\markdown\Markdown();
      }

      return $parser->parse($m);
   }

   /*
    * returns array($phonemes => array($token => array($dockey => $title), ...), ...)
    * key is $phonemes and values is an array with token as key and document array as value
    * document array is 'ns/docid' as doc key and document title as value
    * Search is done recursively from phonemes 9 to 1 until number of result is found.
    * This way returns in preference exact match if possible, then more fuzzy matches.
   */
   public function search($string = '', $nbresult=10, $phonemes = 0){
      if($string === '') return array();
      $keys = explode(' ',$string);
      if(count($keys) == 0) return array();

      foreach($keys as $val){
         if(trim($val) =='') continue;
         $tokens[] = $val ;
      }

      $results = $this->search_metaphone($tokens, $nbresult, $phonemes) ;
      $searchresults = array() ;
      foreach($results as $k => $v){
         foreach($v as $kv => $vv){
            if(in_array(array('title' => $vv, 'link' => $kv), $searchresults) == false){
               $searchresults[] = array('title' => $vv, 'link' => $kv) ;
            }
         }
      }
      return $searchresults ;
   }

   private function search_metaphone($tokens, $nbresult = 10, $phonemes = 0){

      // Create tags/title token cache per namespace if missing, or if documents.yaml newer. return list of cache files
      $caches = $this->update_token_cache();

      // Do search in every cache file
      // can find more than $nbresult allowed, but this will be limited into the twig template
      $results = array($phonemes=>'');
      for($i=0; $i<count($caches); $i++){
         $cache = $caches[$i] ;
         $l = $this->lock_file($cache); // protect file to be changed while accessing it
         // load cache
         try {
               $yaml = Yaml::parse(file_get_contents($cache));
         } catch (ParseException $e) {
               $message = sprintf("Parse error in %s : %s", $cache, $e->getMessage());
               $this->error= $message ;
               return false ;
         }
         $this->unlock_file($l); // unlock
         for($j=0; $j<count($tokens); $j++){
            $token = $tokens[$j];
            foreach($yaml as $ns => $tags){
               // search documents in such namespace
               foreach($tags as $tag => $ids){
                  if( metaphone($token, $phonemes) == metaphone($tag, $phonemes)){
                     foreach($ids as $kid => $id){
                        $results[$phonemes]["$ns/$id"] = $this->get_doc_title($ns, $id);
                     }
                  }
               }
            }
         }
      }
      $newresults = array();
      // Search more fuzzy results if needed
      if(count($results[$phonemes]) < $nbresult){
         if($phonemes === 0) $phonemes = 10;
         $newphonemes = $phonemes-- ;
         if($newphonemes === 0) return $results ; // return anyway with less results than allowed
         $newresults = $this->search_metaphone($tokens, ($nbresult - count($results)), $newphonemes);
      }
      return array_merge($results, $newresults);
   }

   private function update_token_cache(){
      $caches = array();
      // List namespace's document list
      $nss = glob($this->ns_path."/*");
      foreach($nss as $k => $v){
         if(!is_dir($v)) continue;
         $ns    = basename($v);
         $df    = $v."/documents.yaml" ;
         if(!file_exists($df)) continue;
         $cache = $this->cache_path."/".$ns.".tags" ;
         // Create cache if missing or document file newer
         if(!file_exists($cache) or (filemtime($df) > filemtime($cache))){
            // Load document
            $doc = $this->check_yaml($df) ;
            if(!is_array($doc)) continue ;
            $ic=array();
            foreach($doc['bysha1'] as $dk => $dv){
               // Get tags and title words > 3 lowercase letters (keep short acronyms)
               $title  = $dv['title'] ;
               $id     = $dv['id'] ;
               $_words = preg_split("/ /",preg_replace("/[^A-Za-z0-9]/"," ",$title),-1,PREG_SPLIT_NO_EMPTY);
               foreach($_words as $kw => $vw){
                  if(strlen($vw) > 3 or (strtoupper($vw) === $vw)){
                     $words[] = $vw ;
                  }
               }
               $tags = array_merge($dv['tags'], $words);
               // create cache index
               foreach($tags as $kt => $vt){
                  if(in_array($id,$ic[$ns][strtolower($vt)])) continue ;
                  $ic[$ns][strtolower($vt)][]= $id ;
               }
            }
            // Write cache
            $dumper = new Dumper();
            $cache_yaml = $dumper->dump($ic, 3);
            file_put_contents("$cache", $cache_yaml);
         }
         $caches[] = $cache ;
      }
      return $caches ;
   }

   private function lock_file($file = NULL){
      $fp = fopen($file, "r+");
      if (flock($fp, LOCK_EX)) {
         return $fp ;
      }
   }

   private function unlock_file($fp){
      flock($fp, LOCK_UN);
      fclose($fp);
   }

   public function get_hash_ns($mixed = NULL){
      if($mixed === NULL) return false;
      if(is_array($mixed)){
         $res = array();
         foreach($mixed as $v){
            $res[] = $this->get_hash_ns($v);
         }
         return $res;
      }else{
         return hash('crc32', trim($mixed));
      }
   }

   public function get_namespace_from_hashns($ns = NULL){
      if($ns === NULL) return "";
      $ns_index = $this->data_path.'/ns/namespaces.yaml' ;
      $tmp = $this->safe_read_doc($ns_index);
      return $tmp["$ns"] ;
   }

}

?>
