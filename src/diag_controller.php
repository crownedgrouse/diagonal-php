<?php

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

//******************************************************************************
//**** DIAGNOSTIC                                                           ****
//******************************************************************************
$app->match('/diag', function () use ($app) {
   if($app['debug']===true)error_log(__LINE__." match /diag");
    $data = array(
        'Search' => ''
    );
   // Display fuzzy search in tags, document title
       $form = $app['form.factory']
        ->createBuilder('form', $data)
        ->add('Search')
        ->getForm()
    ;

    $request = $app['request'];
    $requests = array();

    $color = "blue" ;

    if ($request->isMethod('POST')) {
        $form->bind($request);
        if ($form->isValid()) {
               $data = $form->getData();
               $D = new diagonal($app['diagonal.path']) ;
               $results = $D->search($data['Search']);
        }
    }

    $response =  $app['twig']->render(
        'search.html.twig',
        array(
            'results' => $results,
            'form' => $form->createView()
        )
    );

    return $response;
}, 'GET|POST');

$app->get('/diag/{ns}', function ($ns) use ($app) {
    if($app['debug']===true)error_log(__LINE__." get /diag/$ns");
    return  $app->redirect("/list/$ns");
});


$app->get('/diag/{ns}/{doc}', function ($ns, $doc) use ($app) {
   if($app['debug']===true)error_log(__LINE__." get /diag/$ns/$doc");
   // All availables questions in such document
   $D = new Diagonal($app['diagonal.path']);

   $docsha1 = $D->get_doc_sha1($ns, $doc);

   $docpath = $D->ns_path."/$ns/$docsha1" ;
   $docarray = $D->safe_read_doc($docpath) ;
   $header = $docarray['doc'] ;

   if(!is_array($header)){
      return $app->redirect("/edit/$ns/$doc");
   }
   // Replace non string values
   $header = array_map(array('Diagonal','replace_non_strings'), $header);

   $quests = array_filter($docarray, array('Diagonal','list_texts'));
   // Bring only Id and Text of questions
   while( list($k, $v) =each($quests) ){
      if($k[0]==='?'){
            $k[0]='/'; // override ? by /+
            $questions[$k] = $v['text'] ;
      }
   }
   $rights=$D->rights ;

   return $app['twig']->render('question.html.twig',  array('context'   => "/diag/$ns/$doc",
                                                            'short_ns'  => "$ns",
                                                            'rights'    => $rights,
                                                            'document'  => $header,
                                                            'questions' => $questions));
});

$app->get('/diag/{ns}/{doc}', function ($ns, $doc) use ($app) {
    if($app['debug']===true)error_log(__LINE__." get /diag/{ns}/{doc}");
    // Ask the start question
    // NB : a question MUST not be called ?0 in documents, or only the start one
    // Get the start question in document from index
    $D = new Diagonal($app['diagonal.path']);
    $docsha1 = $D->get_doc_sha1($ns, $doc);

    $startq = $docs['bysha1'][$docsha1]['start'] ;
    $qid1 = substr($startq,1,strlen($startq)) ;

    return $app->redirect("/diag/$ns/$doc/$qid1");
});

$app->post('/diag/{ns}/{doc}/{qid}', function ($ns, $doc, $qid, Request $request) use ($app) {
   if($app['debug']===true)error_log(__LINE__." post /diag/$ns/$doc/$qid");
   // Analyse answer to question
   $answer = $app['request']->get($qid) ;
   // Get associated value to answer : question or diagnostic ?
   $D = new Diagonal($app['diagonal.path']);

   $docsha1  = $D->get_doc_sha1($ns, $doc);

   $docpath  = $D->ns_path."/$ns/$docsha1" ;
   $docarray = $D->safe_read_doc($docpath) ;
   $question = $docarray["?".$qid] ;

   foreach($question['options'] as $k => $v){
      if(isset($v[$answer]) and is_array($v[$answer])){
         foreach($v[$answer] as $response => $link){
            $value = $link ;
         }
         break;
      }elseif(isset($v[$answer])){
            $value = $v[$answer];
         break;
      }
   }
   $diag = NULL ;
   $diagcontent='';
   $diaglist = array();

   if(is_array($value)){ // Several possible diagnostics
      // Pick up ^diags content if the case
      foreach ($value as $d){
            $pref = substr($d, 0, 1) ;
            $rest = substr($d, 1, strlen($d)) ;
         if($pref == '^'){ // A link to a diagnostic
            $diaglist[] = $docarray["^".$rest] ;
         }else{ // A text
            $diaglist[] = $d ;
         }
      }
   }else{ // A question, or a diagnostic or a link to another doc
      $pref = substr($value, 0, 1) ;
      $rest = substr($value, 1, strlen($value)) ;

      if($pref == '?'){ // Another question : redirect to question
            return  $app->redirect("/diag/$ns/$doc/$rest");
      }elseif($pref == '^'){ // A diagnostic : redirect to diagnostic
            return  $app->redirect("/diag/$ns/$doc/$qid/$rest");
      }elseif($pref == '@'){ // A redirect to another document (optionnaly from another namespace)
         $pos = strpos($rest, ':') ;
         if( $pos === false){
                  return  $app->redirect("/diag/$ns/$rest");
         }else{
                  $nspref = substr($rest, 0, $pos) ;
                  $newns = $D->get_nshash_from_prefix($ns, $docsha1, $nspref) ;
                  if(trim($newns)=='') return new Response("The requested page could not be found : namespace not declared.");

                  return  $app->redirect("/diag/$newns/$rest");
         }
      }else{ // A direct diagnostic : render
         $diag = NULL ;
         $diagcontent = $D->markdown2html($value, $app['diagonal.markdown.flavor']);
      }
   }

   // Display
   $data = array( 'namespace' => "$ns",
                  'document'  => "$doc",
                  'qid'       => "$qid",
                  'diaglist'  => $diaglist,
                  'diag'      => "$diag",
                  'content'   => $diagcontent);

   $form = $app['form.factory']->createBuilder('form', $data)
            ->add('namespace')
            ->add('document')
            ->add('qid')
            ->add('diaglist')
            ->getForm();

   $form->handleRequest($request);

   return $app['twig']->render('diag.data.html.twig', $data);

});

$app->get('/diag/{ns}/{doc}/{qid}', function ($ns, $doc, $qid, Request $request) use ($app) {
   if($app['debug']===true)error_log(__LINE__." get /diag/$ns/$doc/$qid");
   // Ask a particular question
   $D = new Diagonal($app['diagonal.path']);

   $docsha1 = $D->get_doc_sha1($ns, $doc);

   $docpath  = $D->ns_path."/$ns/$docsha1" ;
   $docarray = $D->safe_read_doc($docpath) ;
   $question = $docarray["?$qid"] ;

   $data = array( 'namespace' => "$ns",
                  'document'  => "$doc",
                  'qid'       => "$qid",
                  'question'  => $question);

   $form = $app['form.factory']->createBuilder('form', $data)
         ->add('namespace')
         ->add('document')
         ->add('qid')
         ->add('question')
         ->getForm();

   $form->handleRequest($request);

   return $app['twig']->render('diag.html.twig', $data);
});


$app->match('/diag/{ns}/{doc}/{qid}/{diag}', function ($ns, $doc, $qid, $diag, Request $request) use ($app) {
   if($app['debug']===true)error_log(__LINE__." match /diag/{ns}/{doc}/{qid}/{diag}");
   // Ask a particular question
   $D = new Diagonal($app['diagonal.path']);

   $docsha1 = $D->get_doc_sha1($ns, $doc);

   $docpath  = $D->ns_path."/$ns/$docsha1" ;
   $docarray = $D->safe_read_doc($docpath) ;
   $question = $docarray["?$qid"] ;
   $diag_raw = $docarray["^$diag"] ;

   // Translate possible markdown sintax
   $diagcontent = $D->markdown2html($diag_raw, $app['diagonal.markdown.flavor']);

   $data = array( 'namespace' => "$ns",
                  'document'  => "$doc",
                  'qid'       => "$qid",
                  'question'  => $question,
                  'diag'      => "$diag",
                  'content'   => $diagcontent);

   $form = $app['form.factory']->createBuilder('form', $data)
         ->add('namespace')
         ->add('document')
         ->add('qid')
         ->add('diag')
         ->add('content')
         ->getForm();

   $form->handleRequest($request);

   return $app['twig']->render('diag.data.html.twig', $data);
}, 'GET|POST');

?>
