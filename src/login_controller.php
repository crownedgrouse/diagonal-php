
<?php
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
//******************************************************************************
//**** RUN                                                                  ****
//******************************************************************************
$app->get('/', function () use ($app) {

    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('/login');
    }

    return $app['twig']->render('index.html.twig', array());
});

$app->get('/logout', function () use ($app) {
    $app['session']->set('user');
    return $app->redirect('/');
});

$app->match('/login', function (Request $request) use ($app) {

    $data = array(
        'login' => '',
        'password' => '',
    );

    $form = $app['form.factory']->createBuilder('form', $data)
        ->add('login')
        ->add('password','password')
        ->getForm();

    $form->handleRequest($request);

    if ($form->isValid()) {
        $data = $form->getData();
        $email = $data['login'];
        $password = $data['password'];

        // Check the user
        $D = new Diagonal($app['diagonal.path']);
        $D->auth_method = $app['diagonal.auth.method'];
        if($D->authenticate($email, $password) === true){
            $app['session']->set('user', array('login' => $email));
            // Load rights
            $app['session']->set('rights', $D->get_rights($email));
            // Store owned namespaces
            $app['session']->set('owned_namespaces', $app['diagonal.namespaces']);
            $app['session']->set('owned_hash_ns', $D->get_hash_ns($app['diagonal.namespaces']));
            // redirect
            return $app->redirect('/');
        }else{
            // redirect
            return $app->redirect('/login');
        }
    }

    return $app['twig']->render('login.html.twig', array('form' => $form->createView()));
});

?>
