<?php
//******************************************************************************
//**** API                                                                  ****
//******************************************************************************
$app->get('/api', function() use ($app) {
    return new Response('return list of API available', 200);
});

$app->get('/api/json', function() use ($app) {
    return $app->json(array('json' => '1.0'));
});

$app->get('/api/yaml', function() use ($app) {
    return Yaml::dump(array('yaml' => '1.0'));
});
?>
